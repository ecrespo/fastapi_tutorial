import os

from debug_toolbar.middleware import \
    DebugToolbarMiddleware  # pylint: disable=E0401
from fastapi import APIRouter, FastAPI  # pylint: disable=E0401

from app.apps.demo import routers as routers_demo
from app.config import settings
from app.utils.loggerfactory import LoggerFactory

tags_metadata = [
    {
        'name': 'user',
        'description': 'This is user route',
    },
    {
        'name': 'products',
        'description': 'This is products route',
    },
]
app_name = os.path.basename(__file__).replace('.py', '')

app = FastAPI(
    title=settings.TITLE,
    debug=settings.DEBUG,
    openapi_tags=tags_metadata,
    openapi_url='/api/v1/openapi.json',
    # doc_url="/api/v1/docs",
    redoc_url='/api/v1/redoc',
    description='API to get, create, update and delete users.',
    version=settings.VERSION,
    terms_of_service='https://example.com/terms/',
    contact={
        'name': settings.AUTHOR,
        'url': settings.URL,
        'email': settings.EMAIL,
    },
    license_info={
        'name': 'Apache 2.0',
        'url': 'https://www.apache.org/licenses/LICENSE-2.0.html',
    },
)

app.add_middleware(DebugToolbarMiddleware)

logger = LoggerFactory.get_logger(app_name, log_level='INFO')
api_router = APIRouter()


@app.on_event('startup')
async def startup_event():  # dead: disable
    """Startup event."""
    logger.info('Startup event.')


@app.on_event('shutdown')
async def shutdown_event():  # dead: disable
    """Shutdown event."""
    logger.info('Shutdown')


app.include_router(routers_demo.api_router)
