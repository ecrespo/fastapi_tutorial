from enum import Enum

import strawberry  # pylint: disable=E0401


class TypeResponse(str, Enum):
    orjson = 'orjson'
    json = 'json'
    ujson = 'ujson'


@strawberry.type
class User:  # pylint: disable=R0903
    name: str
    age: int


@strawberry.type
class Query:  # pylint: disable=R0903
    @strawberry.field  # pylint: disable=R0201,R0903
    def user(self) -> User:  # pylint: disable=R0903,R0201
        return User(name='Patrick', age=100)  # type: ignore
