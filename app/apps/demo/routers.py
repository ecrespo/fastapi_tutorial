"""Main application for fastAPI."""
# mypy: ignore-errors
# https://dev.to/ericchapman/python-fastapi-crash-course-533e
import os
from pathlib import Path
from typing import Optional

import strawberry  # pylint: disable=E0401
from fastapi import Cookie  # pylint: disable=E0401
from fastapi import Header  # pylint: disable=E0401
from fastapi import APIRouter, Request, status  # pylint: disable=E0401
from fastapi.responses import JSONResponse  # pylint: disable=E0401
from fastapi.responses import ORJSONResponse  # pylint: disable=E0401
from fastapi.responses import UJSONResponse  # pylint: disable=E0401
from fastapi.staticfiles import StaticFiles  # pylint: disable=E0401
from strawberry.asgi import GraphQL  # pylint: disable=E0401

from app.apps.demo.schemas import Query, TypeResponse
from app.utils.loggerfactory import LoggerFactory

STATIC_DIR = Path(__file__).parent.parent.resolve() / 'demo' / 'static'

app_name = os.path.basename(__file__).replace('.py', '')

logger = LoggerFactory.get_logger(app_name, log_level='INFO')

schema = strawberry.Schema(query=Query)


graphql_app = GraphQL(schema)


api_router = APIRouter(prefix='/demo', tags=['demo'])

api_router.mount('/static', StaticFiles(directory=STATIC_DIR), name='static')


@api_router.get('/request_header')
def read_headers(  # dead: disable
    sso_cookie: Optional[str] = Cookie(
        None, alias='sso'
    ),  # pylint: disable=W0613 # dead: disable
    sso_token: Optional[str] = Header(None),  # pylint: disable=W0613
):
    """Read headers."""
    logger.info('Read headers.')
    return {'sso token': sso_token}


@api_router.get('/client-data')
def client_data(request: Request) -> dict:  # dead: disable
    """Get client data."""
    logger.info('Get client data.')
    client_host = request.client.host
    client_port = request.client.port

    return {'client_host': client_host, 'client_port': client_port}


@api_router.get('/', status_code=status.HTTP_200_OK)
def read_main():  # dead: disable
    """Read main."""
    logger.info('Read Main')
    print(STATIC_DIR)
    return {'msg': 'Hello World'}


@api_router.get('/response/{response}')
def response_type(response: TypeResponse = TypeResponse.json):
    out = {
        'Hello': 'World',
    }
    result = (
        ORJSONResponse(content=out)
        if response == 'orjson'
        else UJSONResponse(content=out)
        if response == 'ujson'
        else JSONResponse(content=out)
    )  # noqa: E501
    return result


@api_router.get('/images')
def images():
    """List images from static folder"""
    out = []
    for filename in os.listdir(f'{STATIC_DIR}/images'):
        out.append(
            {'name': filename.split('.')[0], 'path': '/static/images/' + filename}
        )
    return JSONResponse(out)


@api_router.get('/mp3s')
def mp3s():
    """List mp3 files from static folder"""
    out = []
    for filename in os.listdir(f'{STATIC_DIR}/mp3s'):
        out.append({'name': filename.split('.')[0], 'path': '/static/mp3s/' + filename})
    return JSONResponse(out)


@api_router.get('/items/{item_id}')
async def read_item(item_id: int):
    return {'item_id': item_id}


api_router.add_route('/demo/graphql', graphql_app)
api_router.add_websocket_route('/demo/graphql', graphql_app)
