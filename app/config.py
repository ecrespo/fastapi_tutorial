from pathlib import Path
from typing import List, Literal, Optional, Union

from confz import (ConfZ, ConfZEnvSource,  # pylint: disable=E0401
                   ConfZFileSource)
from pydantic import AnyUrl, SecretStr  # pylint: disable=E0401

CONFIG_DIR = Path(__file__).parent.parent.resolve() / 'config'
print(CONFIG_DIR)


class Settings:  # pylint: disable=R0903
    TITLE = 'app_name'
    VERSION = '0.0.1'
    DESCRIPTION = 'API to get, create, update and delete users.'
    NAME = 'app_name'
    DEBUG = True
    EMAIL = 'ecrespo@gmail.com'
    AUTHOR = 'Ernesto Crespo'
    URL = 'https://www.seraph.to'


settings = Settings()


class AppConfig(ConfZ):  # pylint: disable=R0903
    title: str
    version: str
    cors_origins: List[AnyUrl]

    CONFIG_SOURCES = ConfZFileSource(file=CONFIG_DIR / 'api.yml')


class SQLiteDB(ConfZ):  # pylint: disable=R0903
    type: Literal['sqlite']
    path: Optional[Path]  # None if in-memory


class PostgreSQL(ConfZ):  # pylint: disable=R0903
    type: Literal['postgresql']
    user: str
    password: SecretStr
    host: str
    database: str


DBTypes = Union[SQLiteDB, PostgreSQL]


class DBConfig(ConfZ):  # pylint: disable=R0903
    echo: bool
    db: DBTypes

    CONFIG_SOURCES = [
        ConfZFileSource(folder=CONFIG_DIR, file_from_env='DB_ENV'),
        ConfZEnvSource(allow=['db.user', 'db.password']),
    ]
