"""Factory Logger."""
# https://medium.com/geekculture/create-a-reusable-logger-factory-for-python-projects-419ad408665d
import logging


class LoggerFactory:  # pylint: disable=R0903
    """Class LoggerFactory."""

    _LOG = None

    @staticmethod
    def __create_logger(log_file, log_level):
        """Create Logger."""
        # set the logging format
        log_format = '%(asctime)s:%(levelname)s:%(message)s'

        # Initialize the class variable with logger object
        LoggerFactory._LOG = logging.getLogger(log_file)
        logging.basicConfig(
            level=logging.INFO, format=log_format, datefmt='%Y-%m-%d %H:%M:%S'
        )

        # set the logging level based on the user selection
        if log_level == 'INFO':
            LoggerFactory._LOG.setLevel(logging.INFO)
        elif log_level == 'ERROR':
            LoggerFactory._LOG.setLevel(logging.ERROR)
        elif log_level == 'DEBUG':
            LoggerFactory._LOG.setLevel(logging.DEBUG)
        return LoggerFactory._LOG

    @staticmethod
    def get_logger(log_file, log_level):
        """Get Looger."""
        logger = LoggerFactory.__create_logger(log_file, log_level)

        # return the logger object
        return logger
