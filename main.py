import os

import uvicorn  # pylint: disable=E0401

if __name__ == '__main__':
    app_name = os.path.basename(__file__).replace('.py', '')
    app = f'app.{app_name}:app'
    uvicorn.run(
        app, host='0.0.0.0', port=8085, reload=True, workers=2, log_level='info'
    )
